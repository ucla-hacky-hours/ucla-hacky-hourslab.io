---
date: "2019-12-18"
title: "Schedule"
---
This quarter (Fall 2021), we run a collaborative workspace
every **Wednesday from 3-5pm over Gather Town**. Please email
mcowen at g dot ucla dot edu for the link to join the virtual space.
Please come join us to work, collaborate, seek feedback or help, or
simply to enjoy friendly company! 

In addition to these weekly coworking sessions, we are organizing the following workshops:  
- **January 18-19, 2020**: Free two-day Software Carpentry workshop, led by Madeline Cowen and Gaurav Kandlikar, 9am-4.30pm, La Kretz Garden Pavilion. [Link](https://ucla-hacky-hours.gitlab.io/2020-01-18-SWC/).
- **January 23, 2020**: Workshop on analysis of movement data for ecology, led by [Dr. Rachel Blakey](https://www.ioes.ucla.edu/person/rachel-blakey/). 5-6.30pm, TLSB 1100.

*More TBA...*
